TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    BucketQueue.cpp \
    EDTMapBase.cpp \
    FixedMap.cpp

HEADERS += \
    BucketQueue.h \
    common.h \
    EDTMapBase.h \
    FixedMap.h
