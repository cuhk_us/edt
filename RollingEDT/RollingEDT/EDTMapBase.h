#ifndef MAPBASE_H
#define MAPBASE_H
#include <EDTMapUtilities.h>
#include <BucketQueue.h>

class EDTMapBase
{
protected:
    struct relativeNeighbour
    {
        EDTMap::coord shift;
        char parentDir;
        float shift_dist;
    };

public:
    EDTMapBase();
    virtual ~EDTMapBase();
    void setObstacle(const EDTMap::coord &s);
    void removeObstacle(const EDTMap::coord &s);
    void updateDistanceMap();
    void setMapSpecs(const EDTMap::pos & origin, const double & gridstep);
    EDTMap::pos getOrigin();
    double getGridStep();
    EDTMap::coord pos2coord(const EDTMap::pos & p);
    float costAt(const EDTMap::coord &, const float default_value);
    unsigned char occupancyAt(const EDTMap::coord &, const unsigned char default_value);
    virtual std::list<EDTMap::coord> rayCast(const EDTMap::pos & p0, const EDTMap::pos & p1)=0;
    virtual std::list<std::list<EDTMap::coord>> trajectoryCast(const std::list<EDTMap::pos> &)=0;
    virtual EDTMap::gridValue* data(const EDTMap::coord &) = 0;

protected:
    void raise(const EDTMap::coord &s);
    void lower(const EDTMap::coord &s);

protected:
    BucketQueue* _ptr_raise_buckets;
    BucketQueue* _ptr_lower_buckets;
    relativeNeighbour *_nbs;
    EDTMap::pos _origin;
    double _gridstep;
};

#endif // MAPBASE_H
