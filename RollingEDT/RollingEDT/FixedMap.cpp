#include "FixedMap.h"
#include <stdio.h>
#include <string.h>
FixedMap::FixedMap(int maxX, int maxY, int maxZ):
    EDTMapBase(),
    _maxX(maxX),
    _maxY(maxY),
    _maxZ(maxZ)
{
    _map = new EDTMap::gridValue[_maxX*_maxY*_maxZ];
}

FixedMap::~FixedMap()
{
    delete[] _map;
}

EDTMap::gridValue* FixedMap::data(const EDTMap::coord &s)
{
    if (s.x<0 || s.x>=_maxX ||
            s.y<0 || s.y>=_maxY ||
            s.z<0 || s.z>=_maxZ)
        return nullptr;

    return &_map[s.z*_maxX*_maxY+s.y*_maxX+s.x];
}

std::list<std::list<EDTMap::coord>> FixedMap::trajectoryCast(const std::list<EDTMap::pos> &posList)
{
    std::list<std::list<EDTMap::coord>> output;

    if(posList.size()>1) //Only start processing when there are at leat two points (one line-segment)
    {
        std::list<EDTMap::pos>::const_iterator ptr1;
        std::list<EDTMap::pos>::const_iterator ptr2 = posList.begin();
        for (unsigned int i=0;i<posList.size()-1;i++)
        {
            // Increase the iterator
            ptr1 = ptr2;
            ptr2 = std::next(ptr1);

            // Insert to output
            output.push_back(rayCast(*ptr1, *ptr2));
        }
    }
    return output;
}

std::list<EDTMap::coord> FixedMap::rayCast(const EDTMap::pos &p0, const EDTMap::pos &p1)
{
    std::list<EDTMap::coord> ray;
    EDTMap::coord p0Index = pos2coord(p0);
    EDTMap::coord p1Index = pos2coord(p1);

    ray.push_back(p0Index);
    // same grid, we are done
    if(p0Index.x == p1Index.x && p0Index.y == p1Index.y && p0Index.z == p1Index.z)
    {
        return ray;
    }
    // Initialization phase ------------------------
    EDTMap::pos direction = p1 - p0;
    double length = sqrt(direction.square());
    direction = direction / length; //normalize the vector

    int    step[3];
    double tMax[3];
    double tDelta[3];

    EDTMap::coord currIndex = p0Index;
    for (unsigned int i=0; i<3; i++)
    {
        // compute step direction
        if(direction.at(i) > 0.0) step[i] = 1;
        else if (direction.at(i) < 0.0) step[i] = -1;
        else step[i] = 0;

        // compute tMax, tDelta
        if(step[i] != 0)
        {
            double voxelBorder = double(currIndex.at(i)) * _gridstep +
                    _origin.at(i) + double(step[i]) * _gridstep*0.5;
            tMax[i] = (voxelBorder - p0.const_at(i))/direction.at(i);
            tDelta[i] = _gridstep / fabs(direction.at(i));
        }
        else
        {
            tMax[i] =  std::numeric_limits<double>::max( );
            tDelta[i] = std::numeric_limits<double>::max( );
        }
    }

    // Incremental phase -----------------------------------
    bool done = false;
    while (!done)
    {
        unsigned int dim;

        // find minimum tMax;
        if (tMax[0] < tMax[1]){
            if (tMax[0] < tMax[2]) dim = 0;
            else                   dim = 2;
        }
        else {
            if (tMax[1] < tMax[2]) dim = 1;
            else                   dim = 2;
        }

        // advance in drection "dim"
        currIndex.at(dim) += step[dim];
        tMax[dim] += tDelta[dim];
        ray.push_back(currIndex);

        // reached endpoint?
        if(currIndex.x == p1Index.x && currIndex.y == p1Index.y && currIndex.z == p1Index.z)
        {
            done = true;
            break;
        }
        else
        {
            double dist_from_origin = std::min(std::min(tMax[0], tMax[1]), tMax[2]);

            if(dist_from_origin > length)
            {
                done = true;
                break;
            }
        }
    }
    return ray;
}

void FixedMap::copyData(EDTMap::gridValue* data, int maxX, int maxY, int maxZ,
                        const EDTMap::pos &origin, const double &gridstep)
{
    if (maxX != _maxX || maxY != _maxY || maxZ != _maxZ)
    {
        printf("Dimension mismatch during map data copying!\n Map is not copied!\n");
        return;
    }
    if (!(origin == _origin))
    {
        printf("Origin mismatched!\n Map is not copied!\n");
        return;
    }
    if (gridstep != _gridstep)
    {
        printf("Gridstep mismatched!\n Map is not copied!\n");
        return;
    }
    memcpy (_map, data, sizeof(EDTMap::gridValue)*_maxX*_maxY*_maxZ);
}

EDTMap::gridValue* FixedMap::getMapPtr()
{
    return _map;
}

int FixedMap::getMaxX()
{
    return _maxX;
}

int FixedMap::getMaxY()
{
    return _maxY;
}

int FixedMap::getMaxZ()
{
    return _maxZ;
}
