#include "BucketQueue.h"

BucketQueue::BucketQueue(int bucket_size):
    _queue_size(0),
    _bucket_size(bucket_size),
    _head_pos(bucket_size-1)
{
    _bucket = new std::list<EDTMap::coord>[bucket_size];
}

BucketQueue::~BucketQueue()
{
    delete[] _bucket;
}

void BucketQueue::insert(int key, const EDTMap::coord &val, EDTMap::queuePos& qPos)
{
    if (key >=0 && key<_bucket_size)
    {
        qPos.it = _bucket[key].insert(_bucket[key].end(), val);
        qPos.bucPos = key;
        _queue_size++;
        if (key < _head_pos)
        {
            _head_pos = key;
        }
    }
}

void BucketQueue::insert(float key, const EDTMap::coord& val, EDTMap::queuePos &qPos)
{
    return insert(EDTMap::grid_dist_to_int(key),val,qPos);
}

void BucketQueue::remove(EDTMap::queuePos & qPos)
{
    _bucket[qPos.bucPos].erase(qPos.it);
    qPos.reset();
    _queue_size--;
}

EDTMap::coord BucketQueue::front(bool &isvalid)
{
    EDTMap::coord val;
    isvalid = false;
    for (int i=_head_pos; i<_bucket_size; i++)
    {
        if (!_bucket[i].empty())
        {
            isvalid = true;
            val = _bucket[i].front();
            _head_pos = i;
            break;
        }
    }
    return val;
}
