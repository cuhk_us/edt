#ifndef BUCKETQUEUE_H
#define BUCKETQUEUE_H

#include <list>
#include <EDTMapUtilities.h>

class BucketQueue
{
public:
    BucketQueue(int bucket_size);
    ~BucketQueue();
    void insert(int key, const EDTMap::coord& val, EDTMap::queuePos& qPos);
    void insert(float key, const EDTMap::coord& val, EDTMap::queuePos& qPos);
    EDTMap::coord front(bool & isvalid);
    void remove(EDTMap::queuePos &qPos);
    int queueSize() {return _queue_size;}

private:
    int _queue_size;
    int _bucket_size;
    int _head_pos;
    std::list<EDTMap::coord>* _bucket;
};

#endif // BUCKETQUEUE_H
