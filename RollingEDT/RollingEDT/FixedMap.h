#ifndef FIXEDMAP_H
#define FIXEDMAP_H
#include <EDTMapBase.h>

class FixedMap:public EDTMapBase
{
public:
    FixedMap(int maxX, int maxY, int maxZ);
    virtual ~FixedMap();
    void copyData(EDTMap::gridValue* data,int maxX, int maxY, int maxZ,
                  const EDTMap::pos & origin, const double & gridstep);
    virtual EDTMap::gridValue* data(const EDTMap::coord &s);
    virtual std::list<EDTMap::coord> rayCast(const EDTMap::pos & p0, const EDTMap::pos & p1);
    virtual std::list<std::list<EDTMap::coord>> trajectoryCast(const std::list<EDTMap::pos> & posList);

    EDTMap::gridValue* getMapPtr();
    int getMaxX();
    int getMaxY();
    int getMaxZ();
private:
    int _maxX,_maxY,_maxZ;
    EDTMap::gridValue* _map;
};

#endif // FIXEDMAP_H
