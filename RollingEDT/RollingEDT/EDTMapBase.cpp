#include "EDTMapBase.h"
#include <iostream>

EDTMapBase::EDTMapBase()
{
    _ptr_raise_buckets = new BucketQueue(EDTMap::BUCKET_SIZE);
    _ptr_lower_buckets = new BucketQueue(EDTMap::BUCKET_SIZE);

    // Construct the neighbour list
    _nbs = new relativeNeighbour[EDTMap::NB_SIZE];
    int n = (EDTMap::NB_RANGE*2+1); // side length of neighbouring volume
    int i = 0;
    for (int x=-EDTMap::NB_RANGE; x<=EDTMap::NB_RANGE; x++)
    {
        for (int y=-EDTMap::NB_RANGE; y<=EDTMap::NB_RANGE; y++)
        {
            for (int z=-EDTMap::NB_RANGE; z<=EDTMap::NB_RANGE; z++)
            {
                if (x==0 && y==0 && z==0)
                {
                    continue;
                }
                _nbs[i].shift.x = x;
                _nbs[i].shift.y = y;
                _nbs[i].shift.z = z;

                //shift distance
                _nbs[i].shift_dist = sqrt(x*x+y*y+z*z);

                //Calculate the parent direction of this shift (assume <0,0,0> is the parent)
                _nbs[i].parentDir = EDTMap::NB_SIZE-(x+EDTMap::NB_RANGE)
                        -(y+EDTMap::NB_RANGE)*n-(z+EDTMap::NB_RANGE)*n*n;
                i++;
            }
        }
    }

    // Initialize the origin and grid step
    _origin.x = 0;
    _origin.y = 0;
    _origin.z = 0;
    _gridstep = 0.2;
}

EDTMapBase::~EDTMapBase()
{
    delete _ptr_raise_buckets;
    delete _ptr_lower_buckets;
    delete[] _nbs;
}

void EDTMapBase::raise(const EDTMap::coord & s)
{
    EDTMap::coord nb_coord;
    EDTMap::gridValue* ptr_value;
    float dist_n;
    for (int i=0;i<EDTMap::NB_SIZE;i++)
    {
        nb_coord = s+_nbs[i].shift;
        ptr_value = data(nb_coord);
        if (ptr_value == NULL) // only do this if data can be found
        {
            continue;
        }
        if (ptr_value->parentDir != EDTMap::NO_PARENT)
        {
            // remember the distance value before it is cleraed
            dist_n = ptr_value->grid_dist;
            // if s is the parent of its neighbour
            if (ptr_value->parentDir == _nbs[i].parentDir)
            {
                ptr_value->resetEDT();

                // Only add in if not contained by the queue
                if (ptr_value->raiseQPos.bucPos==EDTMap::NOT_IN_Q)
                {
                    _ptr_raise_buckets->insert(dist_n,nb_coord,ptr_value->raiseQPos);
                }
                else
                {
                    _ptr_raise_buckets->remove(ptr_value->raiseQPos);
                    _ptr_raise_buckets->insert(dist_n,nb_coord,ptr_value->raiseQPos);
                }
            }
            else
            {
                // Only add in if not contained by the queue
                if (ptr_value->lowerQPos.bucPos==EDTMap::NOT_IN_Q)
                {
                    _ptr_lower_buckets->insert(dist_n,nb_coord,ptr_value->lowerQPos);
                }
                else
                {
                    _ptr_lower_buckets->remove(ptr_value->lowerQPos);
                    _ptr_lower_buckets->insert(dist_n,nb_coord,ptr_value->lowerQPos);
                }
            }
        }
    }
}

void EDTMapBase::lower(const EDTMap::coord &s)
{
    EDTMap::coord nb_coord;
    EDTMap::gridValue* ptr_value;
    EDTMap::gridValue* s_value = data(s);
    if (s_value == NULL)
    {
        return;
    }
    float grid_dist;
    for (int i=0;i<EDTMap::NB_SIZE;i++)
    {
        nb_coord = s+_nbs[i].shift;
        ptr_value = data(nb_coord);
        if (ptr_value == NULL) // only do this if data can be found
        {
            continue;
        }

        grid_dist = s_value->grid_dist + _nbs[i].shift_dist;
        if (grid_dist < ptr_value->grid_dist)
        {
            ptr_value->grid_dist = grid_dist;
            ptr_value->parentDir = _nbs[i].parentDir;

            // Only add in if not contained by the queue
            if (ptr_value->lowerQPos.bucPos==EDTMap::NOT_IN_Q)
            {
                _ptr_lower_buckets->insert(ptr_value->grid_dist,nb_coord,ptr_value->lowerQPos);
            }
            else
            {
                _ptr_lower_buckets->remove(ptr_value->lowerQPos);
                _ptr_lower_buckets->insert(ptr_value->grid_dist,nb_coord,ptr_value->lowerQPos);
            }
        }
    }
}

void EDTMapBase::setObstacle(const EDTMap::coord &s)
{
    EDTMap::gridValue* s_value = data(s);
    if(s_value == NULL || (s_value->lowerQPos.bucPos!=EDTMap::NOT_IN_Q))
        return;

    s_value->grid_dist = 0.0f;
    s_value->parentDir = EDTMap::SELF_PARENT;
    _ptr_lower_buckets->insert(0.0f,s,s_value->lowerQPos);
}

void EDTMapBase::removeObstacle(const EDTMap::coord &s)
{
    EDTMap::gridValue* s_value = data(s);
    if(s_value == NULL || (s_value->raiseQPos.bucPos!=EDTMap::NOT_IN_Q))
        return;

    s_value->resetEDT();
    _ptr_raise_buckets->insert(0.0f,s,s_value->raiseQPos);
}

void EDTMapBase::updateDistanceMap()
{
    int i=0;
    bool isvalid;
    EDTMap::coord s;
    EDTMap::gridValue* s_value;

    // process the raise queue (remove)
    while(_ptr_raise_buckets->queueSize() > 0)
    {
        i++;
        s = _ptr_raise_buckets->front(isvalid);
        s_value = data(s);
        if (!isvalid || s_value == NULL)
            continue;
        _ptr_raise_buckets->remove(s_value->raiseQPos);

        raise(s);
    }

    // process the lower queue (set)
    while(_ptr_lower_buckets->queueSize() > 0)
    {
        i++;
        s = _ptr_lower_buckets->front(isvalid);
        s_value = data(s);
        if (!isvalid || s_value == NULL)
            continue;
        _ptr_lower_buckets->remove(s_value->lowerQPos);

        lower(s);
    }

    std::cout<<i<<std::endl;
}

EDTMap::coord EDTMapBase::pos2coord(const EDTMap::pos & p)
{
    EDTMap::coord output;
    output.x = floor( (p.x - _origin.x) / _gridstep + 0.5);
    output.y = floor( (p.y - _origin.y) / _gridstep + 0.5);
    output.z = floor( (p.z - _origin.z) / _gridstep + 0.5);
    return output;
}

void EDTMapBase::setMapSpecs(const EDTMap::pos & origin, const double & gridstep)
{
    _origin = origin;
    _gridstep = gridstep;
}

EDTMap::pos EDTMapBase::getOrigin()
{
    return _origin;
}

double EDTMapBase::getGridStep()
{
    return _gridstep;
}

float EDTMapBase::costAt(const EDTMap::coord & grid, const float default_value)
{
    EDTMap::gridValue* gv = data(grid);
    if (gv)
    {
        return gv->grid_dist;
    }
    else
    {
        return default_value;
    }
}

unsigned char EDTMapBase::occupancyAt(const EDTMap::coord & grid, const unsigned char default_value)
{
    EDTMap::gridValue* gv = data(grid);
    if (gv)
    {
        return gv->occupy;
    }
    else
    {
        return default_value;
    }
}
