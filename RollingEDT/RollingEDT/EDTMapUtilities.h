#ifndef EDTMAPUTILITIES_H
#define EDTMAPUTILITIES_H
#include <limits>
#include <cmath>
#include <list>
#include <assert.h>

namespace EDTMap
{

static int grid_dist_to_int(float in)
{
    return std::floor(in);
}

static const float MAX_GRID_DIST = 10; // the maximum distance measured in grid number
static const int BUCKET_SIZE = grid_dist_to_int(MAX_GRID_DIST)+1; // the bucket queue bucket size
static const int NB_RANGE = 1; // the range of neighbours, 1 means 26 neighbours in 3D
static const int NB_SIZE = (NB_RANGE*2+1)*(NB_RANGE*2+1)*(NB_RANGE*2+1)-1; // the range of neighbours, 1 means 26 neighbours in 3D
static const char NO_PARENT = 127;
static const char SELF_PARENT = -1;
static const short int NOT_IN_Q = -1;

struct coord
{
    int x,y,z;
    coord():x(0),y(0),z(0)
    {

    }

    coord operator+(coord const& rhs) const
    {
        coord tmp;
        tmp.x = x + rhs.x;
        tmp.y = y + rhs.y;
        tmp.z = z + rhs.z;
        return tmp;
    }

    coord operator-(coord const& rhs) const
    {
        coord tmp;
        tmp.x = x - rhs.x;
        tmp.y = y - rhs.y;
        tmp.z = z - rhs.z;
        return tmp;
    }

    coord& operator=(coord const& rhs)
    {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }

    int& at(int i)
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
        }
    }
};
//---
struct pos
{
    double x,y,z;
    pos():x(0.0),y(0.0),z(0.0)
    {

    }

    double square()
    {
        return x*x+y*y+z*z;
    }

    pos operator+(pos const& rhs) const
    {
        pos tmp;
        tmp.x = x + rhs.x;
        tmp.y = y + rhs.y;
        tmp.z = z + rhs.z;
        return tmp;
    }

    pos operator-(pos const& rhs) const
    {
        pos tmp;
        tmp.x = x - rhs.x;
        tmp.y = y - rhs.y;
        tmp.z = z - rhs.z;
        return tmp;
    }

    pos operator/(double const& rhs) const
    {
        pos tmp;
        tmp.x = x / rhs;
        tmp.y = y / rhs;
        tmp.z = z / rhs;
        return tmp;
    }

    pos& operator=(pos const& rhs)
    {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }

    bool operator==(pos const& rhs) const
    {
        if (x==rhs.x && y==rhs.y && z==rhs.z)
            return true;
        else
            return false;
    }

    double& at(int i)
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
        }
    }

    const double& const_at(int i) const
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
        }
    }
};
//---
struct queuePos
{
    short int bucPos;
    std::list<coord>::iterator it;
    queuePos():bucPos(NOT_IN_Q)
    {

    }

    void reset()
    {
        bucPos = NOT_IN_Q;
    }
};
//---
struct gridValue
{
    float grid_dist; // the distance measured in grid number
    unsigned char occupy;
    char parentDir; // from 0 ~ 26 are valid values
    queuePos raiseQPos;
    queuePos lowerQPos;

    // parentDir value range -1~NB_SIZE: the NB_SIZE neighbours [0~NB_SIZE] and itself [-1]
    // NO_PARENT = 127 indicates the grid has no parent (is cleared)
    gridValue():grid_dist(MAX_GRID_DIST),occupy(0),parentDir(NO_PARENT)
    {

    }

    void resetEDT()
    {
        grid_dist = MAX_GRID_DIST;
        parentDir = NO_PARENT;
    }

    void resetOccupy()
    {
        occupy = 0;
    }
};
}
#endif // EDTMAPUTILITIES_H
