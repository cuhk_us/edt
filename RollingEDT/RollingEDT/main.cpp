#include <iostream>
#include <chrono>  // for high_resolution_clock
#include <FixedMap.h>
#include <fstream>
#include <assert.h>
#include <chrono>  // for high_resolution_clock
using namespace std;

int main()
{
    ofstream myfile;
    myfile.open("test.txt");
    FixedMap testMap(100,100,1);
    srand(1);
    EDTMap::coord c;
//    for (int i=0;i<100;i++)
//    {
//        c.x = i;
//        c.y = i;
//        c.z = i/5;

//        if (testMap.data(c))
//        {
//            testMap.data(c)->occupy=255;
//        }
//        testMap.setObstacle(c);

//    }
//    testMap.updateDistanceMap();
////---
//    auto start = std::chrono::high_resolution_clock::now();



//    for (int i=0;i<100;i++)
//    {
//        c.x = i;
//        c.y = i;
//        c.z = i/5;

//        if (testMap.data(c))
//        {
//            testMap.data(c)->occupy=0;
//        }
//        testMap.removeObstacle(c);

//    }

//    for (int i=0;i<100;i++)
//    {
//        c.x = i+25;
//        c.y = i;
//        c.z = i/5;

//        if (testMap.data(c))
//        {
//            testMap.data(c)->occupy=255;
//        }
//        testMap.setObstacle(c);

//    }
//    testMap.updateDistanceMap();
//        auto finish = std::chrono::high_resolution_clock::now();
//        std::chrono::duration<double> elapsed = finish - start;
//        std::cout << "Elapsed time: " << elapsed.count() << " s\n";
//    for (int x=0;x<100;x++)
//    {
//        for (int y=0;y<100;y++)
//        {
//            for (int z=0;z<1;z++)
//            {
//                c.x=x;
//                c.y=y;
//                c.z=z;
//                if (testMap.data(c)->occupy==255)
//                {
//                    if (rand()%10>2)
//                    {
//                        testMap.removeObstacle(c);
//                        testMap.data(c)->occupy = 0;
//                    }
//                }
//            }
//        }
//    }
//    testMap.updateDistanceMap();
//---


    for (int i=0;i<100;i++)
    {
        //---
        c.x = 0;
        c.y = i;
        for (int z=0;z<1;z++)
        {
            c.z = z;
            if (testMap.data(c))
            {
                testMap.data(c)->occupy=255;
            }
            testMap.setObstacle(c);
        }
        //---
        c.x = i;
        c.y = 0;
        for (int z=0;z<1;z++)
        {
            c.z = z;
            if (testMap.data(c))
            {
                testMap.data(c)->occupy=255;
            }
            testMap.setObstacle(c);
        }
        //---
        c.x = 99;
        c.y = i;
        for (int z=0;z<1;z++)
        {
            c.z = z;
            if (testMap.data(c))
            {
                testMap.data(c)->occupy=255;
            }
            testMap.setObstacle(c);
        }
        //---
        c.x = i;
        c.y = 99;
        for (int z=0;z<1;z++)
        {
            c.z = z;
            if (testMap.data(c))
            {
                testMap.data(c)->occupy=255;
            }
            testMap.setObstacle(c);
        }
    }

    auto start = std::chrono::high_resolution_clock::now();
    testMap.updateDistanceMap();
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";

////---

    for (int x=0;x<100;x++)
    {
        for (int y=0;y<100;y++)
        {
            for (int z=0;z<1;z++)
            {
                c.x=x;
                c.y=y;
                c.z=z;
                if (testMap.data(c)->occupy==255)
                {
                    if (rand()%10>2)
                    {
                        testMap.removeObstacle(c);
                        testMap.data(c)->occupy = 0;
                    }
                }
                else
                {
                    if (rand()%10>8)
                    {
                        testMap.setObstacle(c);
                        testMap.data(c)->occupy = 255;
                    }
                }
            }
        }
    }

    start = std::chrono::high_resolution_clock::now();
    testMap.updateDistanceMap();
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";


//===================================
    for (int x=0;x<100;x++)
    {
        for (int y=0;y<100;y++)
        {
            for (int z=0;z<1;z++)
            {
                c.x=x;
                c.y=y;
                c.z=z;
//                if (testMap.data(c)->inLowerQ || testMap.data(c)->inRaiseQ)
//                {
//                    assert(0);
//                }
//                if (testMap.data(c)->occupy==255)
//                    myfile<<140<<" ";
//                else
                    myfile<<testMap.data(c)->grid_dist<<" ";
            }
        }
    }
    myfile<<"\n";
    myfile.close();
    std::cout<<"Finish"<<std::endl;
    return 0;
}
